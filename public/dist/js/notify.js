const Notify = {
    success: function (configs = {}) {
        const defaultConfig = {
            title: null,
            icon: 'success',
            timer: 1000,
            showConfirmButton: false,
            allowOutsideClick: false,
        };

        configs = {...defaultConfig, ...configs};

        return Swal.fire(configs);
    },
    error(configs = {}) {
        const defaultConfig = {
            title: 'Error 500',
            text: null,
            icon: 'error',
            allowOutsideClick: false,
            showCancelButton: false
        };

        configs = {...defaultConfig, ...configs}

        return Swal.fire(configs)
    },
    delete(callback, configs = {}) {
        const defaultConfig = {
            title: 'Xác nhận xóa',
            html: '<p>Bạn sắp xóa 1 mục. Điều này là không thể đảo ngược.</p><p>Bạn có chắc chắn muốn xóa?</p>',
            icon: 'error',
            showCancelButton: true,
            cancelButtonText: 'Cancel',
            confirmButtonText: 'Delete',
            confirmButtonColor: '#dc3545',
            reverseButtons: true,
            allowOutsideClick: false,
            showLoaderOnConfirm: true,
            preConfirm: function () {
                return new Promise(function () {
                    return callback();
                });
            }
        };

        configs = {...defaultConfig, ...configs};
        return Swal.fire(configs);
    }
}
