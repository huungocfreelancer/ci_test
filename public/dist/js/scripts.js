$(document).ready(function () {
    'use strict';

    $.fn.dataTable.ext.errMode = "throw";

    var table = $('#user-list-table').DataTable({
        ordering: true,
        autoWidth: true,
        scrollCollapse: true,
        scrollY: true,
        processing: true,
        destroy: true,
        async: true,
        serverSide: true,
        order: [4, 'desc'],
        ajax: {
            url: 'base/api/list',
            dataType: 'json',
            method: 'POST',
            data: function (data) {
                data.csrf_token = $('meta[name="csrf_token"]').attr("content");
                data.filter = $("#filter-form").serializeArray();
            }
        },
        columns: [
            {data: 'fullname'},
            {
                data: 'date_of_birth', render: function (data) {
                    return moment(data).format('DD/MM/YYYY')
                }
            },
            {data: 'gender'},
            {data: 'balancer'},
            {
                data: 'created_at', render: function (data) {
                    return moment(data * 1000).format('DD/MM/YYYY');
                }
            }
        ]
    });

    $("#filter-form").on('input change', function () {
        table.draw();
    });

    /*== Button loader function ==*/
    function buttonLoader(buttonSelector, flag = true) {
        if (buttonSelector.find("i[class^='fa']").length > 0) {
            buttonSelector.find("i[class^='fa']").hide();
        }

        const loaderClass = 'fas fa-spin fa-spinner';

        let loaderHtml = `<i class="${loaderClass} mr-1"></i>`
        if (flag === true) {
            buttonSelector.prepend(loaderHtml);
        } else {
            buttonSelector.find("i[class^='fa']").show();
            buttonSelector.find(`i[class*="${loaderClass}"]`).remove();
        }
        buttonSelector.prop('disabled', flag);
    }

    /*== Ajax function ==*/
    function doAjax(action, method = 'POST', data = {}, callback = null) {
        $.ajax({
            url: action,
            method: method,
            data: data,
            dataType: 'JSON',
            beforeSend: function () {
                buttonLoader($('button:submit'));
            }
        }).done(function (result) {
            Notify.success({title: result.message});
            if (callback === null) {
                return window.location.reload();
            }

            if (typeof callback === 'function') {
                return callback(result);
            }
        }).fail(function (error) {
            console.log(error);
            Notify.error({
                title: 'Error ' + error.status,
                text: error.responseJson?.message || error.statusText
            });
        }).always(function () {
            buttonLoader($('button:submit'), false);
        });
    }

    /*== Form create submit event ==*/
    $("form#create-form").submit(function (e) {
        e.preventDefault();
        const _this = $(this);

        let action, method, data;
        action = $(this).attr('action');
        method = $(this).attr('method');
        data = $(this).serialize();

        return doAjax(action, method, data, function () {
            $("#create-modal").modal('hide');
            table.draw();
            _this[0].reset();
        });

        return false;
    });
});
