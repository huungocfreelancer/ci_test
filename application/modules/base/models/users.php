<?php

class Users extends CI_Model
{
    function __construct()
    {
        // Call the Model constructor
        parent::__construct();
    }

    public function user_list($request = [])
    {
        if (!empty($request['_id'])) {
            return $this->mongo_db->where('_id', $request['_id'])->find_one('users');
        }

        $query = $this->mongo_db;

        // Filter fullname
        if (!empty($request['search']['value'])) {
            $regex = "/".$request['search']['value']."/im";
            $query->where('fullname', new MongoRegex($regex));
        }

        // Filter birthday
        if (!empty($request['filter'][0]['value'])) {
            $query->where(
                $request['filter'][0]['name'],
                $request['filter'][0]['value']
            );
        }

        // Filter gender
        if (!empty($request['filter'][1]['value'])) {
            $query->where(
                $request['filter'][1]['name'],
                $request['filter'][1]['value']
            );
        }

        // Filter balancer
        if (!empty($request['filter'][2]['value'])) {
            $query->where(
                $request['filter'][2]['name'],
                $request['filter'][2]['value']
            );
        }

        // Filter created date
        if (!empty($request['filter'][3]['value'])) {
            $query->where(
                $request['filter'][3]['name'],
                strtotime($request['filter'][3]['value'])
            );
        }

        // Order
        $column = !empty($request['columns'][$request['order'][0]['column']]['data'])
                    ? $request['columns'][$request['order'][0]['column']]['data']
                    : '_id';
        $sortBy = !empty($request['order'][0]['dir']) ? $request['order'][0]['dir'] : 'desc';
        $query->order_by([$column => $sortBy]);

        // Pagination
        $query->offset(!empty($request['start']) ? $request['start'] : 0)
            ->limit(!empty($request['length']) ? $request['length'] : 10);

        $data = $query->get('users');

        return $data;
    }

    public function user_create($data)
    {
        if (empty($data)) {
            return false;
        }

        $query = $this->mongo_db->insert('users', $data);
        return $query;
    }
}
