<!doctype html>
<html lang="vi">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <meta
        name="<?php echo $this->security->get_csrf_token_name(); ?>"
        content="<?php echo $this->security->get_csrf_hash(); ?>"
    />
    <title>User list</title>
    <base href="<?= base_url() ?>"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.13.0/css/all.min.css">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css"
          integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
    <link rel="stylesheet" href="//cdn.datatables.net/1.10.20/css/jquery.dataTables.min.css"/>
    <link rel="stylesheet" href="public/dist/css/style.css"/>
</head>
<body>

<div class="container">
    <h1 style="margin-top: 50px;">User list</h1>
    <div class="mt-5">
        <a href="#create-modal" class="btn btn-primary" data-toggle="modal">
            <i class="fas fa-plus mr-1"></i>
            Create user
        </a>
        <button class="btn btn-info" data-toggle="collapse" data-target="#filter-collapse">
            <i class="fas fa-filter mr-1"></i>
            Filter
        </button>
        <div class="collapse mt-2" id="filter-collapse">
            <div class="card card-body">
                <form id="filter-form">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">Date of birth</label>
                                <input type="date" class="form-control" name="date_of_birth" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">Gender</label>
                                <select name="gender" class="form-control">
                                    <option value="">Select gender</option>
                                    <option value="Male">Male</option>
                                    <option value="Female">Female</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">Balancer</label>
                                <input type="number" min="0" class="form-control" name="balancer" />
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label for="date_of_birth">Created date</label>
                                <input type="date" class="form-control" name="created_at" />
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <div class="table-responsive mt-3 mb-5">
        <table class="table table-hover" id="user-list-table">
            <thead>
                <tr>
                    <th>Fullname</th>
                    <th>Date of birth</th>
                    <th>Genger</th>
                    <th>Balancer</th>
                    <th>Created date</th>
                </tr>
            </thead>
        </table>
    </div>

    <!-- Create modal -->
    <div class="modal fade" tabindex="-1" role="dialog" id="create-modal">
        <div class="modal-dialog modal-xl" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title">Create user</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <form action="base/api/create" method="POST" id="create-form">
                        <input type="hidden" name="<?= $this->security->get_csrf_token_name() ?>" value="<?= $this->security->get_csrf_hash() ?>" />
                        <div class="form-group row">
                            <label for="fullname" class="required col-form-label col-md-2">
                                Fullname
                            </label>
                            <div class="col-md-10">
                                <input type="text" name="fullname" id="fullname" class="form-control" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="date_of_birth" class="required col-form-label col-md-2">
                                Date of birth
                            </label>
                            <div class="col-md-10">
                                <input type="date" name="date_of_birth" id="date_of_birth" class="form-control" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-form-label col-md-2">
                                Gender
                            </label>
                            <div class="col-md-10">
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="gender-male" value="Male" checked>
                                    <label class="form-check-label" for="gender-male">Male</label>
                                </div>
                                <div class="form-check form-check-inline">
                                    <input class="form-check-input" type="radio" name="gender" id="gender-female" value="Female">
                                    <label class="form-check-label" for="gender-female">Female</label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="required col-form-label col-md-2" for="balancer">
                                Balancer
                            </label>
                            <div class="col-md-10">
                                <input type="number" value="0" min="0" name="balancer" id="balancer" class="form-control" required />
                            </div>
                        </div>
                        <div class="form-group row">
                            <div class="col-md-10 offset-md-2">
                                <button type="submit" class="btn btn-info btn-lg">
                                    <i class="fas fa-save mr-1"></i>
                                    Save
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script
    src="https://code.jquery.com/jquery-3.4.1.min.js"
    integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
    crossorigin="anonymous"></script>
<script type="text/javascript" src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/sweetalert2@9"></script>
<script type="text/javascript" src="//cdn.datatables.net/1.10.20/js/jquery.dataTables.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.17.1/moment.js"></script>
<script type="text/javascript" src="public/dist/js/notify.js"></script>
<script type="text/javascript" src="public/dist/js/scripts.js"></script>
</body>
</html>
