<?php
class Base extends MY_Controller{
	function __construct()
    {
		parent::__construct();
        $this->load->helper('url');
	}

    /**
     * Get list the user
     * @return mixed
     */
	public function index()
    {
		return $this->load->view('base');
	}
}
?>
