<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
require APPPATH . '/libraries/REST_Controller.php';

class Api extends REST_Controller {
	function __construct()
    {
		parent::__construct();
        $this->users = $this->load->model('users');
	}

    /**
     * List the user
     */
	public function list_post ()
    {
        $request = $this->post();

        $data = $this->users->user_list($request);

        return $this->responseApi('List success', $data);
	}

    /**
     * Create user
     */
	public function create_post ()
    {
        $validation = [
            [
                'field' => 'fullname',
                'label' => 'Fullname',
                'rules' => 'required'
            ],
            [
                'field' => 'date_of_birth',
                'label' => 'Date of birth',
                'rules' => 'required|date'
            ],
            [
                'field' => 'gender',
                'label' => 'Gender',
                'rules' => 'required'
            ],
            [
                'field' => 'balancer',
                'label' => 'Balancer',
                'rules' => 'required|integer|greater_than_equal_to[0]'
            ]
        ];
        $this->form_validation->set_rules($validation);

        if (!$this->form_validation->run()) {
            return $this->responseServerError();
        }

        $data = $this->post();
        $data['created_at'] = strtotime(date('Y-m-d'));

        $result = $this->users->user_create($data);
        if (empty($result)) {
            return $this->responseServerError();
        }

        return $this->responseApi(
            'Create success',
            $this->users->user_list(['_id' => $result])
        );
	}

    /**
     * Response API message
     *
     * @param string $message
     * @param int $code
     * @param null $data
     */
	protected function responseApi($message = '', $data = null, $code = 200)
    {
        $response = [
            'message' => $message,
            'data' => $data
        ];

        return $this->response($response, $code);
    }

    /**
     * Response server error
     */
    protected function responseServerError()
    {
        return $this->responseApi('Internal server error', null, 500);
    }
}

?>
